FROM rtucek/nginx-php

COPY ./ /usr/local/nginx/html/

COPY ./conf/nginx.conf /usr/local/nginx/conf/nginx.conf
COPY ./conf/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY ./conf/www.conf /usr/local/etc/www.conf

ARG MR_ANDERSON_API_FQDN
ENV MR_ANDERSON_API_FQDN ${MR_ANDERSON_API_FQDN:-api.mr-anderson.johanvo.me}
RUN echo "MR_ANDERSON_API_FQDN = ${MR_ANDERSON_API_FQDN}" >> .env
