<?php

namespace Anderson;

use Assert\AssertionFailedException;
use Exception;
use InvalidArgumentException;
use PestJSON;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

class AgentController
{
    private $agentRepo;

    /**
     * AgentController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $apiFqdn = getenv('MR_ANDERSON_API_FQDN')?: 'api.mr-anderson.johanvo.me';
        $this->agentRepo = new AgentRestRepository(
            new PestJSON($apiFqdn)
        );
    }

    public function createAgent(
        ServerRequestInterface $request
    ): ResponseInterface
    {
        $requestBody = json_decode($request->getBody()->getContents(), false);

        if (null === $requestBody) {
            throw new InvalidArgumentException('Creating a new resource without content is not allowed');
        }

        $this->agentRepo->withNextId(
            static function (int $id) use ($requestBody) {
                return new Agent($id, $requestBody->name);
            }
        );

        return new EmptyResponse();
    }

    public function readAgent(
        /** @noinspection PhpUnusedParameterInspection */
        ServerRequestInterface $request,
        array $args
    ): ResponseInterface
    {
        try {
            $agent = $this->agentRepo->ofId($args['id']);

        } catch (InvalidArgumentException $e) {
            return new EmptyResponse(404);
        } catch (Exception $e) {
            return new EmptyResponse(500);
        }

        if (null === $agent) {
            return new EmptyResponse(404);
        }

        return new JsonResponse($agent->toArray());
    }

    public function list(
        /** @noinspection PhpUnusedParameterInspection */
        ServerRequestInterface $request,
        array $args
    ): ResponseInterface
    {
        $params = $request->getQueryParams();
        $page = is_numeric($params['page']) ? $params['page'] : 1;
        $perPage = is_numeric($params['per-page']) ? $params['per-page'] : 10;
        $start = ($page - 1) * $perPage;

        $agents = $this->agentRepo->getAll();
        $agents = array_slice($agents, $start, $perPage);
        array_walk($agents, static function (Agent &$agent) {
            /** @noinspection CallableParameterUseCaseInTypeContextInspection */
            $agent = $agent->toArray();
        });

        return new JsonResponse($agents);
    }

    /**
     * @param ServerRequestInterface $request
     * @param array $args
     * @return ResponseInterface
     * @throws AssertionFailedException
     */
    public function updateAgent(
        /** @noinspection PhpUnusedParameterInspection */
        ServerRequestInterface $request,
        array $args
    ): ResponseInterface
    {
        $agent = $this->agentRepo->ofId($args['id']);

        if (null === $agent) {
            return new EmptyResponse(404);
        }

        $requestBody = json_decode($request->getBody()->getContents(), false);

        if (null === $requestBody) {
            return new EmptyResponse(500);
        }

        $agent->setName($requestBody->name);

        $this->agentRepo->update($agent);

        return new EmptyResponse();
    }

    public function deleteAgent(
        /** @noinspection PhpUnusedParameterInspection */
        ServerRequestInterface $request,
        array $args
    ): ResponseInterface
    {
        $agent = $this->agentRepo->ofId($args['id']);

        if (null === $agent) {
            return new EmptyResponse(404);
        }

        $this->agentRepo->delete($agent);

        return new EmptyResponse();
    }
}