<?php

namespace Anderson;

interface AgentRepositoryInterface
{
    public function withNextId(callable $callable): void;

    /**
     * @param int $id
     * @return Agent
     * @throws \InvalidArgumentException
     */
    public function ofId(int $id): Agent;

    public function getAll(): array;

    public function update(Agent $agent): void;

    public function delete(Agent $agent): void;
}
