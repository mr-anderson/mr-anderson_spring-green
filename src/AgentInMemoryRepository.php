<?php

namespace Anderson;

class AgentInMemoryRepository implements AgentRepositoryInterface
{
    /** @var Agent[] */
    private static $agents = [];

    public static function add100Agents(): void
    {
        $repo = new self();

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach (range(1, 100) as $i) {
            $repo->withNextId(
                function (int $id) {
                    $name = str_repeat((string)$id, 20);
                    $name = substr($name, 0, 20);
                    return new Agent($id, $name);
                }
            );
        }
    }

    public function withNextId(callable $callable): void
    {
        $this->add(
            $callable(count(self::$agents) + 1)
        );
    }

    private function add(Agent $agent): void
    {
        if (null !== self::$agents[$agent->getId()]) {
            throw new \InvalidArgumentException("Already a resource present with id {$agent->getId()}");
        }

        self::$agents[$agent->getId()] = $agent;
    }

    /**
     * @param int $id
     * @return Agent
     * @throws \InvalidArgumentException
     */
    public function ofId(int $id): Agent
    {
        if (null === self::$agents[$id]) {
            throw new \InvalidArgumentException("No resource found with id {$id}");
        }

        return self::$agents[$id];
    }

    public function getAll(): array
    {
        return self::$agents;
    }

    public function update(Agent $agent): void
    {
        $this->ofId($agent->getId());

        self::$agents[$agent->getId()] = $agent;
    }

    public function delete(Agent $agent): void
    {
        $this->ofId($agent->getId());

        unset(self::$agents[$agent->getId()]);
    }
}