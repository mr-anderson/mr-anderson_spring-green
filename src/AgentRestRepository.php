<?php

namespace Anderson;

use Assert\AssertionFailedException;
use Pest_NotFound;


class AgentRestRepository implements AgentRepositoryInterface
{
    /** @var \Pest */
    private $endPoint;

    /**
     * @param \Pest $endPoint
     */
    public function __construct(\Pest $endPoint)
    {
        $this->endPoint = $endPoint;
    }

    public function withNextId(callable $callable): void
    {
        /** @var array $allAgents */
        $allAgents = (array)$this->endPoint->get('/agent');

        $highestId = array_reduce($allAgents, function ($carry, $item) {
            if ($item['id'] <= $carry) {
                return $carry;
            }

            return $item['id'];
        }, 0);

        $this->add(
            $callable($highestId + 1)
        );
    }

    private function add(Agent $agent): void
    {
        try {
            $existingAgent = $this->ofId($agent->getId());
        } catch (\InvalidArgumentException $e) {
        }

        if (!isset($existingAgent)) {
            $this->endPoint->post(
                '/agent',
                $agent->toArray()
            );
        }
    }

    /**
     * @param int $id
     * @return Agent
     * @throws \InvalidArgumentException
     */
    public function ofId(int $id): Agent
    {
        try {
            /** @var \stdClass $response */
            $response = $this->endPoint->get("/agent/{$id}");
        } /** @noinspection PhpRedundantCatchClauseInspection */
        catch (Pest_NotFound $e) {
            $response = null;
        }

        if (null === $response) {
            throw new \InvalidArgumentException("No resource found with id {$id}");
        }

        try {
            return new Agent($response['id'], $response['name']);
        } catch (AssertionFailedException $e) {
            throw new \InvalidArgumentException($e->getMessage());
        }
    }

    public function getAll(): array
    {
        /** @var array $response */
        $response = (array)$this->endPoint->get('/agent');

        $output = array_map(function ($agent) {
            return new Agent($agent['id'], (string)$agent['name']);
        }, $response);

        return $output;
    }

    public function update(Agent $agent): void
    {
        $this->ofId($agent->getId());

        $this->endPoint->put("/agent/{$agent->getId()}", $agent->toArray());
    }

    public function delete(Agent $agent): void
    {
        $this->ofId($agent->getId());

        $this->endPoint->delete("/agent/{$agent->getId()}");
    }
}
