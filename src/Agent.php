<?php

namespace Anderson;

use Assert\Assertion;
use Assert\AssertionFailedException;

class Agent
{
    private $id;
    private $name;

    /**
     * @param int $id
     * @param string $name
     * @throws AssertionFailedException
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->setName($name);
    }

    public function toJSON(): string
    {
        return json_encode($this->toArray()) ?: '';
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws AssertionFailedException
     */
    public function setName(string $name): void
    {
        Assertion::notNull($name);
        $this->name = $name;
    }
}
