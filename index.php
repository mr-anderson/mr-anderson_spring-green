<?php /** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

use Dotenv\Exception\InvalidPathException;

include __DIR__ . '/vendor/autoload.php';

try {
    $dotenv = Dotenv\Dotenv::create(__DIR__);
    $dotenv->load();
} catch (InvalidPathException $e) {
}

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$responseFactory = new Http\Factory\Diactoros\ResponseFactory;

$strategy = new League\Route\Strategy\JsonStrategy($responseFactory);
$router = new League\Route\Router;
$router->setStrategy($strategy);


$router->group('/agent', function (\League\Route\RouteGroup $route) {
    $route->map('POST', '/', 'Anderson\AgentController::createAgent');
    $route->map('GET', '/{id}', 'Anderson\AgentController::readAgent');
    $route->map('GET', '/', 'Anderson\AgentController::list');
    $route->map('PUT', '/{id}', 'Anderson\AgentController::updateAgent');
    $route->map('DELETE', '/{id}', 'Anderson\AgentController::deleteAgent');
});

$response = $router->dispatch($request);

// send the response to the browser
ob_clean();
(new Zend\Diactoros\Response\SapiEmitter)->emit($response);
